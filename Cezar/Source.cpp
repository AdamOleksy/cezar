#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

char zakodujLiczbe(char znak, int KOD)
{
	if (KOD > 9)
		KOD = KOD % 9;

	znak += KOD;

	if (znak > 57)
		return znak -= 10;

	return znak;
}

char zakodujDuzaLitere(char znak, int KOD)
{
	if (KOD > 25)
		KOD = KOD % 25;

	znak += KOD;

	if (znak > 90)
		return znak -= 26;

	return znak;
}

char zakodujMalaLitere(char znak, int KOD)
{
	if (KOD > 25)
		KOD = KOD % 25;
	znak += KOD;
	if ((int)znak > 122)
		return znak -= 26;

	return znak;
}

char zakodujZnak(char znak, int KOD)
{
	
	if (znak >= 48 && znak <= 57)
		return zakodujLiczbe(znak, KOD);

	if (znak >= 65 && znak <= 90)
		return zakodujDuzaLitere(znak, KOD);

	if (znak >= 90 && znak <= 122)
		return zakodujMalaLitere(znak, KOD);

	return znak;
}

std::string zakodujLinijke(std::string &tekst, int KOD)
{
	std::string zakodowany;
	int dlugosc = tekst.length();
	for (int i = 0; i < dlugosc; i++)
	{
		zakodowany += zakodujZnak(tekst[i], KOD);
	}

	return zakodowany;
}

std::vector<std::string> pobierzTekst(std::string zrodlo)
{
	std::string tekst;
	std::fstream plikDoZakodowania;
	std::vector<std::string> strVec;

	plikDoZakodowania.open(zrodlo, std::ios::in);

	if (plikDoZakodowania.good())
	{
		while (!plikDoZakodowania.eof())
		{
			std::string linijkaTeksu;
			std::getline(plikDoZakodowania, linijkaTeksu);
			strVec.push_back(linijkaTeksu);
		}
	}
	else std::cout << "Brak dostepu do pliku!" << std::endl;

	plikDoZakodowania.close();
	return strVec;
}

void czyscPlik(std::string sciezka)
{
	std::ofstream plik;
	plik.open(sciezka);
	plik.clear();
	plik.close();
}

void zapiszTekst(std::string cel, const char* zakodowany, int wielkosc)
{
	std::fstream plikZakodowany;
	plikZakodowany.open(cel, std::ios::app);

	if (plikZakodowany.good())
	{
		plikZakodowany << zakodowany << std::endl;
	}
	else std::cout << "Brak dostepu do docelowego pliku!" << std::endl;
	plikZakodowany.close();
}

void zakodujTekst(std::string zrodlo, std::string cel, int KOD)
{
	czyscPlik(cel);

	std::vector<std::string> calyTekst = pobierzTekst(zrodlo);

	int wielkosc = calyTekst.size();
	for (int i = 0; i < wielkosc; i++)
	{
		std::string linijka = zakodujLinijke(calyTekst.at(i), KOD);
		const char * c = linijka.c_str();
		zapiszTekst(cel, c, calyTekst.at(i).length());
	}

}